from django.apps import AppConfig


class StoryApp(AppConfig):
    name = 'story'
    verbose_name = "Story Builder"
